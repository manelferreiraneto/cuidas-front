import axios from '../utils/axios';
import moment from 'moment';
import { isNil } from 'lodash';

export const createAppointment = async ({ fullname, email, phoneNumber, details, date, timeInMinutes, duration }) => {
  try {
    const formattedDateTime = moment(date, 'YYYY-MM-DD')
      .add(timeInMinutes, 'minute')
      .format('YYYY-MM-DD HH:mm');

    const params = {
      fullname,
      email,
      phoneNumber,
      date: formattedDateTime,
      duration: duration
    }

    if (!isNil(details)) params.details = details;

    const { data } = await axios.post(`/appointment`, params);

    return data;
  } catch (err) {
    throw err;
  }
};

export const getAppointmentAvailableTimes = async () => {
  try {
    const { data } = await axios.get(`/appointment/time_availability`);
    return data;
  } catch (err) {
    throw err;
  }
};

export const getAppointmentList = async () => {
  try {
    const { data } = await axios.get(`/appointment`);
    return data;
  } catch (err) {
    throw err;
  }
}

export const cancelAppointment = async ({ appointmentId }) => {
  try {
    const { data } = await axios.delete(`/appointment/${appointmentId}`);
    return data;
  } catch (err) {
    throw err;
  }
}