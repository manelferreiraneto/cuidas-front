import React from 'react';
import { Route, Switch } from 'react-router-dom';

import SiteWrapper from './components/SiteWrapper/SiteWrapper';

import NewAppointment from './views/NewAppointment/NewAppointment'
import AppointmentList from './views/Admin/AppointmentList'

function App() {
  return (
    <SiteWrapper>
      <Switch>
        <Route path="/" exact component={NewAppointment} />
        <Route path="/admin" exact component={AppointmentList} />
      </Switch>
    </SiteWrapper>
  );
}

export default App;
