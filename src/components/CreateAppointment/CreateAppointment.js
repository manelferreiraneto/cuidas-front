import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import moment from 'moment';
import { get, map } from 'lodash';
import styled from 'styled-components';

const CreateAppointment = (props) => {
  const { values, errors, touched, handleSubmit, handleChange, availableTimes } = props;

  const timeAvailabilityForDate = () => {
    const times = get(availableTimes, values.date, availableTimes.default);

    return map(times, (time) => {
      return (
        <MenuItem value={
          time.minuteOfDay
        }>{ time.formatted }</MenuItem>
      );
    });
  }

  const durationAvailabilityForTime = () => {
    const times = get(availableTimes, values.date, availableTimes.default);
    const durations = get(times, `${values.time}.durations`, availableTimes.default.durations);

    return durations.map((duration) => {
      return (
        <MenuItem value={
          duration
        }>{ `${duration} minutos` }</MenuItem>
      );
    });
  }

  return (
    <Card>
      <CardContent>
        <Typography variant="h6" color="textSecondary">
          Agende uma consulta
        </Typography>
          <StyledRow>
            <StyledCol>
              <TextField
                required
                id="fullname"
                name="fullname"
                label="Seu nome completo"
                defaultValue=""
                margin="normal"
                fullWidth
                error={errors.fullname && touched.fullname}
                onChange={handleChange}
                value={values.fullname}
              />
            </StyledCol>
            <StyledCol>
              <TextField
                required
                id="phoneNumber"
                name="phoneNumber"
                fullWidth
                label="Seu telefone"
                defaultValue=""
                margin="normal"
                error={errors.phoneNumber && touched.phoneNumber}
                onChange={handleChange}
                value={values.phoneNumber}
              />
            </StyledCol>
          </StyledRow>
          <StyledRow>
            <StyledCol>
              <TextField
                required
                id="email"
                name="email"
                label="Seu e-mail"
                defaultValue=""
                margin="normal"
                fullWidth
                onChange={handleChange}
                error={errors.email && touched.email}
                value={values.email}
              />
            </StyledCol>
          </StyledRow>

          <StyledRow>
            <TextField
              id="details"
              name="details"
              label="Detalhes (opcional)"
              placeholder="Insira detalhes, se achar necessário :)"
              defaultValue=""
              multiline
              fullWidth
              margin="normal"
              onChange={handleChange}
              value={values.details}
            />
          </StyledRow>

          <StyledRow margin={`1rem 0rem 0rem 0rem`}>
            <StyledFormControl width={`32%`}>
              <InputLabel htmlFor="date">Data</InputLabel>
              <Select
                labelId="date"
                id="date"
                name="date"
                value={values.date}
                onChange={handleChange}
                error={errors.date && touched.date}
              >
                {
                  availableTimes.datesRange.map((day) => (
                    <MenuItem value={
                      moment(day, 'YYYY-MM-DD').format('YYYY-MM-DD')
                    }>{ moment(day).format('DD/MM/YYYY') }</MenuItem>
                  ))
                }
              </Select>
            </StyledFormControl>
            
            <StyledFormControl width={`32%`}>
              <InputLabel htmlFor="time">Horário</InputLabel>
              <Select
                labelId="time"
                id="time"
                name="time"
                value={values.time}
                onChange={handleChange}
                error={errors.time && touched.time}
              >
                {
                  timeAvailabilityForDate()
                }
              </Select>
            </StyledFormControl>

            <StyledFormControl width={`32%`}>
              <InputLabel htmlFor="duration">Duração</InputLabel>
              <Select
                labelId="duration"
                id="duration"
                name="duration"
                value={values.duration}
                onChange={handleChange}
                error={errors.duration && touched.duration}
              >
                {
                  durationAvailabilityForTime()
                }
              </Select>
            </StyledFormControl>
          </StyledRow>
      </CardContent>
      <CardActions>
        <Button color="primary" onClick={handleSubmit}>Agendar</Button>
      </CardActions>
    </Card>
  );
}

const StyledRow = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap !important;
  ${(props) => `margin:${props.margin}`};
`

const StyledCol = styled.div`
  margin: 0 0.3rem;
  flex-grow: 1;
  ${(props) => props.width ? `width:${props.width}` : null};
`

const StyledFormControl = styled(FormControl)`
  ${(props) => props.width ? `width:${props.width}` : null};
`

export default CreateAppointment;