import React from 'react';
import Snackbar from '@material-ui/core/Snackbar'
import styled from 'styled-components'

const SnackbarWrapper = ({ showing, success, message, onClose }) => {
  return (
    <StyledSnackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right'
      }}
      autoHideDuration={6000}
      aria-describedby="snackbar"
      message={
        <span id="snackbar">
          {message}
        </span>
      }
      open={showing}
      onClose={onClose}
      success={success}
    />
  );
}

const StyledSnackbar = styled(Snackbar)`
  ${(props) => `background-color:${props.success ? 'green' : 'red' }`};
`;

export default SnackbarWrapper;