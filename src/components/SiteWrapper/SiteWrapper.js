import React from 'react';
import Header from '../Header/Header';

import styled from 'styled-components';

const SiteWrapper = (props) => {
  const {
    children
  } = props;

  return (
    <div>
      <Header title="Cuidas" />
      <StyledBody>
        {children}
      </StyledBody>
    </div>
  );
}

const StyledBody = styled.div`
  margin: 5rem 2rem;
`

export default SiteWrapper;