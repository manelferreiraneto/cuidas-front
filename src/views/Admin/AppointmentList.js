import React, { useEffect, useState, Fragment } from 'react';
import { getAppointmentList, cancelAppointment } from '../../services/appointment';

import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Divider from '@material-ui/core/Divider'

import SnackbarWrapper from '../../components/SnackbarWrapper/SnackbarWrapper'

import styled from 'styled-components';
import moment from 'moment';

const AppointmentList = () => {
  const [appointments, setAppointments] = useState([]);
  const [deletedCount, setDeletedCount] = useState(0);
  const [successCancelling, setSuccessCancelling] = useState(false);
  const [showingSnackbar, setShowingSnackbar] = useState(false);
  const onCloseSnackbar = () => setShowingSnackbar(false);

  useEffect(() => {
    const getAppointments = async() => {
      const appointmentList = await getAppointmentList();
      setAppointments(appointmentList);
    }
    getAppointments();
  }, [deletedCount]);

  const handleCancelAppointment = async (appointmentId) => {
    try {
      await cancelAppointment({
        appointmentId
      });

      setDeletedCount(deletedCount + 1);
      setSuccessCancelling(true);
    } catch (e) {
      setSuccessCancelling(false);
      throw e;
    } finally {
      setShowingSnackbar(true);
    }
  };

  return (
    <ListWrapper>
      <StyledCard>
        <CardContent>
          <Typography variant="h6" color="textSecondary">
            Lista de consultas
          </Typography>
          <List>
            {
              appointments.length !== 0 ?
                appointments.map((appointment, index) => (
                <Fragment>
                  <StyledListItem>
                    <ListItemText
                      primary={appointment.patient[0].fullname}
                      secondary={
                        <Fragment>
                          <Typography
                            component="span"
                            variant="body2"
                            color="textPrimary"
                          >
                            {appointment.patient[0].email}
                          </Typography>
                          {` - ${appointment.patient[0].phoneNumber}`}
                          <br></br>
                          {`${moment(appointment.scheduledForDate).format('DD/MM/YYYY - HH:mm')} às 
                            ${moment(appointment.expectedToEndAt).format('HH:mm')}`}
                        </Fragment>
                      }
                    />
                    <ListItemSecondaryAction>
                      <IconButton edge="end" aria-label="delete" onClick={() => handleCancelAppointment(appointment._id)}>
                        <DeleteIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </StyledListItem>
                  {
                    (appointments.length - 1) !== index ?
                      <Divider variant="fullWidth" component="li" />
                    : null
                  }
              </Fragment>
              ))
              : `Não há consultas marcadas :)`
            }
          </List>
        </CardContent>
      </StyledCard>      

      <SnackbarWrapper 
        showing={showingSnackbar} 
        onClose={onCloseSnackbar}
        success={successCancelling}
        message={successCancelling ? 'Consulta cancelada com sucesso :)' : 'Algo deu errado :/'} />
    </ListWrapper>
  );
}

const StyledListItem = styled(ListItem)`
  padding: 1rem 0.75rem !important;
  margin: 0.1rem !important;
`

const StyledCard = styled(Card)`
  flex-grow: 1 !important;
  max-width: 50rem !important;
`

const ListWrapper = styled.div`
  display: flex !important;
  justify-content: center !important;
`

export default AppointmentList;