import React, { useEffect, useState } from 'react';
import CreateAppointment from '../../components/CreateAppointment/CreateAppointment';
import { createAppointment, getAppointmentAvailableTimes } from '../../services/appointment';
import { Formik } from 'formik';
import * as Yup from 'yup';

import SnackbarWrapper from '../../components/SnackbarWrapper/SnackbarWrapper'

import styled from 'styled-components';
import moment from 'moment';

const appointmentSchema = Yup.object().shape({
  fullname: Yup.string()
    .required('Nome é obrigatório')
    .matches(
      /^([a-zA-ZÀ-ŸẽẼ]+)\s{1,}([a-zA-ZÀ-ŸẽẼ]\s*)+$/, 
      'Por favor, preencha seu nome inteiro'
    ),
  email: Yup.string()
    .matches( /^([a-zA-Z0-9]+[\.\-\_]?[a-zA-Z0-9]+)@([a-z]+)(\.[a-z]+)+$/)
    .required('Email é obrigatório'),
  phoneNumber: Yup.string()
    .min(10, 'Número inválido')
    .required('Telefone é obrigatório'),
  date: Yup.date('Data inválida')
    .required('Data de nascimento é obrigatória'),
  time: Yup.number().required(),
  duration: Yup.number().required()
});

const Appointment = () => {
  const [availableTimes, setAvailableTimes] = useState({
    datesRange: [moment().format('YYYY-MM-DD')],
    default: {durations: []}
  });
  const [success, setSuccess] = useState(false);
  const [showingSnackbar, setShowingSnackbar] = useState(false);
  const onCloseSnackbar = () => setShowingSnackbar(false);

  useEffect(() => {
    const getAvailableTimes = async () => {
      const availableTimes = await getAppointmentAvailableTimes();
      setAvailableTimes(availableTimes);
    };
    getAvailableTimes();
  }, [showingSnackbar]);

  const handleCreateAppointment = async (values, { setSubmitting, resetForm }) => {
    try {
      setSubmitting(true);
      
      await createAppointment({
        fullname: values.fullname,
        email: values.email,
        phoneNumber: values.phoneNumber,
        details: values.details,
        date: values.date,
        timeInMinutes: values.time,
        duration: values.duration
      });
      
      setSuccess(true);
      resetForm();
    } catch (e) {
      setSuccess(false);
      throw e;
    } finally {
      setSubmitting(false);
      setShowingSnackbar(true);
    }
  };

  return (
    <StyledAppointment>
      <Formik 
        initialValues={{
          fullname: '',
          email: '',
          phoneNumber: '',
          details: '',
          date: '',
          time: '',
          duration: ''
        }}
        validationSchema={appointmentSchema}
        onSubmit={handleCreateAppointment}
      >
        {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <CreateAppointment 
              values={values}
              errors={errors}
              touched={touched}
              handleSubmit={handleSubmit} 
              handleChange={handleChange}
              availableTimes={availableTimes}
            /> 
          )
        }
      </Formik>

      <SnackbarWrapper 
        showing={showingSnackbar} 
        onClose={onCloseSnackbar}
        success={success}
        message={success ? 'Consulta agendada com sucesso :)' : 'Algo deu errado :/'} />
    </StyledAppointment>
  );
}

const StyledAppointment = styled.div`
  display: flex !important;
  justify-content: center !important;
`

export default Appointment;